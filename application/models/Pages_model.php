<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Pages Model Class
 * @author ivan lubis <ivan.z.lubis@gmail.com>
 * @version 3.0
 * @category Model
 * @desc Pages model
 * 
 */
class Pages_model extends CI_Model
{
    /**
     * constructor
     */
    function __construct()
    {
        parent::__construct();
    }

    function getMetaPageBySlug($slug) {

    	$data = $this->db
    			->where('is_delete', 0)
                ->where('uri_path', $slug)
    			->get('pages')
    			->row_array();

    	return $data;
    }


}